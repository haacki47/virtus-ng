import { VirtusNgPage } from './app.po';

describe('virtus-ng App', function() {
  let page: VirtusNgPage;

  beforeEach(() => {
    page = new VirtusNgPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
