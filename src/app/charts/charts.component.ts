import { Component, OnInit } from '@angular/core';

declare const Chart: any

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.sass']
})
export class ChartsComponent implements OnInit {

  constructor() { }

  public lineChartData:Array<any> = [
   { data: [65, 59, 80, 81, 67, 70, 80],
     label: 'Сотрудники',
     pointRadius: 5,
     pointHoverRadius: 5,
     pointBorderColor: '#2196f3',
     pointBorderWidth: 2,
     pointBackgroundColor: '#fff'
   },
  ];

  public lineChartLabels:Array<any> = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];

  public lineChartOptions:any = {
   responsive: true
 };

  public lineChartColors:Array<any> = [
   {
     backgroundColor: '#28649b',
     borderColor: '#2196f3',
   },
 ];
 public lineChartLegend:boolean = false;
 public lineChartType:string = 'line';

  ngOnInit() {
    Chart.defaults.global.defaultFontColor = '#757da4';
  }

}
