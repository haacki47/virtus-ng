import { Component } from '@angular/core';
import { ChartsComponent } from './charts/charts.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'app works!';
}
