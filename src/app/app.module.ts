import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { ChartsComponent } from './charts/charts.component';
import { WorkflowComponent } from './workflow/workflow.component';
import { RaportComponent } from './raport/raport.component';
import { InboxComponent } from './inbox/inbox.component';
import { ProfileComponent } from './profile/profile.component';


const appRotues: Routes = [
  { path: '', redirectTo: 'charts', pathMatch: 'full' },
  { path: 'charts', component: ChartsComponent },
  { path: 'workflow', component: WorkflowComponent },
  { path: 'raport', component: RaportComponent },
  { path: 'inbox', component: InboxComponent },
  { path: 'profile', component: ProfileComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ChartsComponent,
    WorkflowComponent,
    RaportComponent,
    InboxComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ChartsModule,
    RouterModule.forRoot(appRotues)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
